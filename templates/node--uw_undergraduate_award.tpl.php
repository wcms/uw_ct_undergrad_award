<?php

/**
 * @file
 */
?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>" <?php print $attributes; ?>>
  <div class="node-inner">

    <?php if (!$page): ?>
      <h2<?php print $title_attributes; ?>><a href="<?php print $node_url; ?>"><?php print $title; ?></a></h2>
    <?php endif; ?>

    <?php print $user_picture; ?>

    <?php if ($display_submitted): ?>
      <div class="submitted"><?php print $date; ?> - <?php print $name; ?></div>
    <?php endif; ?>

    <div class="content_node"<?php print $content_attributes; ?>>
      <?php
        // We hide the comments and links now so that we can render them later.
        hide($content['field_undergrad_award_program']);
        hide($content['field_undregrad_award_enroll']);
        hide($content['field_undergrad_award_desc']);
        hide($content['field_undergrad_award_value']);
        hide($content['field_undergrad_award_valuedesc']);
        hide($content['field_undergrad_award_elig']);
        hide($content['field_undergrad_award_ptype']);
        hide($content['field_undergard_award_term']);
        hide($content['field_undergrad_award_deadline']);
        hide($content['field_undergrad_award_add_except']);
        hide($content['field_undergrad_award_except']);
        hide($content['field_undergrad_award_detail']);
        hide($content['field_undergrad_award_spec']);
        hide($content['field_undergrad_award_contact']);
        hide($content['field_undergrad_award_notes']);
        hide($content['comments']);
        hide($content['links']);
        hide($content['service_links']);
        print render($content);

        if (render($content['field_undergrad_award_program']) != "") {
      ?>
        <div class="field"><div class="field-label">Program:</div></div>
        <?php }
        print render($content['field_undergrad_award_program']);
        print render($content['field_undregrad_award_enroll']);
        print render($content['field_undergrad_award_desc']);
        print render($content['field_undergrad_award_value']);
        print render($content['field_undergrad_award_valuedesc']);
        print render($content['field_undergrad_award_elig']);
        print render($content['field_undergrad_award_ptype']);
        print render($content['field_undergard_award_term']);
        print render($content['field_undergrad_award_deadline']);
        print render($content['field_undergrad_award_except']);

        if (render($content['field_undergrad_award_detail']) != "") {
      ?>
        <div class="field"><div class="field-label">Application details:</div></div>
        <?php }
        print render($content['field_undergrad_award_detail']);
        print render($content['field_undergrad_award_spec']);
        print render($content['field_undergrad_award_contact']);
        print render($content['field_undergrad_award_notes']);
      ?>
      <?php $pos = strpos($_SERVER['HTTP_REFERER'], 'search-results?title='); ?>
      <?php if ($pos !== FALSE): ?>
        <a href="<?php echo $_SERVER['HTTP_REFERER'] ?>">Return to Results</a>
      <?php endif;?>
    </div>
    <?php if (!empty($content['links']['terms'])): ?>
      <div class="terms"><?php print render($content['links']['terms']); ?></div>
    <?php endif;?>

    <?php if (!empty($content['links'])): ?>
      <div class="links"><?php print render($content['links']); ?></div>
    <?php endif; ?>
  </div> <!-- /node-inner -->
</div> <!-- /node-->

<?php print render($content['comments']); ?>
<?php print render($content['service_links']); ?>
