<?php

/**
 * @file
 * uw_ct_undergrad_award.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_undergrad_award_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_citizenship.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_citizenship'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_citizenship',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award citizenship',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-citizenship:admin/structure/taxonomy/undergrad_award_citizenship',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_deadline.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_deadline'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_deadline',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award deadline',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-deadline:admin/structure/taxonomy/undergrad_award_deadline',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_detail.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_detail'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_detail',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award application details',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-application-details:admin/structure/taxonomy/undergrad_award_detail',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_enrollment.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_enrollment'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_enrollment',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award enrollment',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-enrollment:admin/structure/taxonomy/undergrad_award_enrollment',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_process.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_process'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_process',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award process type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-process-type:admin/structure/taxonomy/undergrad_award_process',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_program.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_program'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_program',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award program',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-program:admin/structure/taxonomy/undergrad_award_program',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_type.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/undergrad_award_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/undergrad_award_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Undergraduate award type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'identifier' => 'menu-site-manager-vocabularies_undergraduate-award-type:admin/structure/taxonomy/undergrad_award_type',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Undergraduate award application details');
  t('Undergraduate award citizenship');
  t('Undergraduate award deadline');
  t('Undergraduate award enrollment');
  t('Undergraduate award process type');
  t('Undergraduate award program');
  t('Undergraduate award type');

  return $menu_links;
}
