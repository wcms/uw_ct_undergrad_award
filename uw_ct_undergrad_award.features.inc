<?php

/**
 * @file
 * uw_ct_undergrad_award.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_undergrad_award_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
  if ($module == "taxonomy_display" && $api == "taxonomy_display") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_undergrad_award_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_undergrad_award_node_info() {
  $items = array(
    'uw_undergraduate_award' => array(
      'name' => t('Undergraduate Award'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Award name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_rdf_default_mappings().
 */
function uw_ct_undergrad_award_rdf_default_mappings() {
  $schemaorg = array();

  // Exported RDF mapping: uw_undergraduate_award.
  $schemaorg['node']['uw_undergraduate_award'] = array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'field_deadline_using_date' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_date' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_add_except' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_deadline' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_type' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_program' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undregrad_award_enroll' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_citizen' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_elig' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_except' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_ptype' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_spec' => array(
      'predicates' => array(),
    ),
    'field_undergrad_award_detail' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergard_award_term' => array(
      'predicates' => array(),
      'type' => 'rel',
    ),
    'field_undergrad_award_value' => array(
      'predicates' => array(),
    ),
  );

  return $schemaorg;
}
